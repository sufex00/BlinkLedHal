#include"ioMapping.h"

#define CLK 720		/* clock em MHz */

#define delay(mS) for(int i=0 ; i<(mS*CLK); i ++) {}


#define IOWRITE(GPIOx, PIN_x, VALUE); ((GPIO *) GPIOx)->ODR ^= (-VALUE ^ ((GPIO *) GPIOx)->ODR) & (1<<PIN_x);

#define IOSET(GPIOx, PIN_x); ((GPIO *) GPIOx)->ODR |= 1<<PIN_x;

#define IOCLEAR(GPIOx, PIN_x); ((GPIO *) GPIOx)->ODR &= ~(1<<PIN_x);

#define IOTOGGLE(GPIOx, PIN_x); ((GPIO *) GPIOx)->ODR ^= 1<<PIN_x;
